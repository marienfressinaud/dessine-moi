import os
import boopsy as boopsy

if __name__ == "__main__":
    idees_filepath = os.path.join(os.curdir, "idees.txt")
    with open(idees_filepath, "r") as idees_file:
        idees = idees_file.readlines()

    template_index_filepath = os.path.join(os.curdir, "templates", "index.html")
    template_style_filepath = os.path.join(os.curdir, "templates", "style.css")

    output_index_filepath = os.path.join(os.curdir, "public", "index.html")
    output_style_filepath = os.path.join(os.curdir, "public", "style.css")

    template_index = boopsy.Template(template_index_filepath)
    template_style = boopsy.Template(template_style_filepath)

    index_output = template_index.render({"idees": idees})
    style_output = template_style.render({"idees": idees})

    with open(output_index_filepath, "w") as output_file:
        output_file.write(index_output)

    with open(output_style_filepath, "w") as output_file:
        output_file.write(style_output)
