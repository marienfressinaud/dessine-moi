import re


class BoopsySyntaxError(Exception):
    """Exception raised if template file is invalid."""

    pass


class Code:
    """Code is an utility class aimed to help us to manage code generation.

    Example:

    >>> code = Code()
    >>> code.write("foo = 0")
    >>> code.write("if True:")
    >>> code.indent()
    >>> code.write("foo = bar")
    >>> context = code.run({"bar": 42})
    >>> context["foo"]
    42
    """

    INDENT_STEP = 4

    def __init__(self):
        self.code = []
        self.indent_level = 0

    def write(self, line):
        """Write a new line of code."""
        indentation = " " * self.indent_level
        self.code.append(indentation + line)

    def indent(self):
        """Increase the code indentation by one step."""
        self.indent_level += self.INDENT_STEP

    def dedent(self):
        """Decrease the code indentation by one step."""
        self.indent_level -= self.INDENT_STEP

    def run(self, global_context={}):
        """Exec the written code and return a dict containing local context."""
        local_context = {}
        exec("\n".join(self.code), global_context, local_context)
        return local_context


class Template:
    def __init__(self, template_name):
        """Initialize a Boopsy template from file content."""
        self.template_name = template_name
        with open(self.template_name, "r") as f:
            self.template = f.read()

    def render(self, variables):
        """Return a string corresponding to template content with vars replaced."""
        # `code` will store a valid Python mini-program. It will basically
        # create a `rendering` array, storing the strings to render at the end.
        # It will be helpful to handle the `if` and `for` statements because we
        # will not have to manage variable scopes and logic by ourselves: we'll
        # simply rely on the Python behaviour.
        code = Code()
        code.write("rendering = []")

        ops_stack = []

        # The template is splitted in tokens:
        # - {{ a_python_expression }}
        # - {% if a_python_expression %}
        # - or all the other strings
        tokens = re.split(r"(?s)({{.*?}}|{%.*?%})", self.template)
        for token in tokens:
            if token.startswith("{{"):
                expression = token[2:-2].strip()
                if len(expression) == 0:
                    self._oopsy("an expression is expected between {{ }}")

                # Expression token is just valid Python code so we add it to
                # the rendering array. It will be evaluated later.
                code.write(f"rendering.append({expression})")

            elif token.startswith("{%"):
                words = token[2:-2].strip().split()
                statement = words[0]
                args = words[1:]

                if statement == "for":
                    if len(args) < 3:
                        self._oopsy("for statement expects at least 3 arguments")
                    if "in" not in args:
                        self._oopsy("for statement expects arguments to contain 'in'")
                    ops_stack.append("for")
                    code.write(f"for {' '.join(args)}:")
                    code.indent()

                elif statement == "endfor":
                    if len(args) != 0:
                        self._oopsy(f"{statement} expects no argument")
                    if not ops_stack:
                        self._oopsy(f"unexpected {statement} (too many ends)")
                    current_op = statement[3:]
                    expected_op = ops_stack.pop()
                    if expected_op != current_op:
                        self._oopsy(
                            f"unexpected {statement} (should be end{expected_op})"
                        )
                    code.dedent()

                else:
                    self._oopsy(f"unknown statement (got '{statement}')")

            elif token:
                # default tokens must be added to the rendering array
                literal = repr(token)
                code.write(f"rendering.append({literal})")

        if ops_stack:
            self._oopsy(f"missing end statement (expected 'end{ops_stack[-1]}')")

        # `result` will store the `rendering` variable so we can get the finale
        # string to return to the caller.
        context = variables.copy()
        result = code.run(context)

        # `local_context["rendering"]` is now an array containing our
        # interprated template. `item` might be None and so `join` can fail,
        # `str` makes sure we are manipulating only strings.
        return "".join(str(item) for item in result["rendering"])

    def _oopsy(self, message):
        """Raise a BoopsySyntaxError.

        This method has no real interest, it's mainly for the pun / similarity
        between Boopsy and Oopsy (https://en.wiktionary.org/wiki/oopsy).

        It is called when parsing the template.
        """
        raise BoopsySyntaxError(message)
