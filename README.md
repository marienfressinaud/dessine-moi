# Dessine-moi

Dessine-moi est une micro-application Web statique destinée à proposer des
idées aléatoires pour dessiner.

Vous pouvez utiliser Dessine-moi à cette adresse : [marienfressinaud.frama.io/dessine-moi](https://marienfressinaud.frama.io/dessine-moi)

**Important :** je ne prévois pas de travailler plus sur Dessine-moi. Il
s’agissait avant tout de me fournir des idées pour pratiquer le dessin, mais
j’ai arrêté depuis. Le projet est donc terminé, mais n’hésitez pas à reprendre
le code pour votre propre usage.

Le reste de ce document s’adresse aux développeurs et développeuses qui
souhaiteraient reprendre le code.

## Développement

Dessine-moi est extrêmement simple : un script Python ([`main.py`](main.py))
prend un fichier d’idées ([`idees.txt`](idees.txt)) et génère des fichiers
`public/index.html` et `public/style.css` à partir de [fichiers
templates](templates/). Le système de template lui-même tient en quelques
lignes ([boopsy.py](boopsy.py)).

Pour ce qui est de la gestion de l’aléatoire, j’en ai expliqué le
fonctionnement [dans un article](https://marienfressinaud.fr/css-aleatoire.html).

Hormis Python, aucune autre dépendance n’est nécessaire.

Pour générer l’application sous le répertoire `public/`, il suffit d’exécuter
la commande suivante :

```console
$ python main.py
```

Puis de l’ouvrir dans votre navigateur :

```console
$ xdg-open public/index.html
```

## Production

Dessine-moi est déployé automatiquement via Gitlab Pages qui consiste à servir
des sites statiques directement depuis un dépôt Gitlab.

Le code de la branche `main` est ainsi [automatiquement construit](.gitlab-ci.yml)
et servi à l’adresse [marienfressinaud.frama.io/dessine-moi](https://marienfressinaud.frama.io/dessine-moi/).

Pour l’héberger chez vous, il suffit de suivre la même procédure que décrite
dans la section « Développement » est de déposer le répertoire `public/` généré
sur un serveur (ou de l’ouvrir directement dans votre navigateur).

## Licence

[Dessine-moi](https://framagit.org/marienfressinaud/dessine-moi) est placé sous
licence [MIT](LICENSE.txt).
